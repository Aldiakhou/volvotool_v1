#######################################################
#
# swp_tool.py
# Python implementation of the Class SwpTool
# Original author: ext_say
#
#######################################################

# Mandatory imported classes
import time
import argparse
import sys
import openpyxl
from src.vnv_exceptions import SwpError
from src.vnv_tool_config import VnvToolConf
from src.slaves.bwc import BWC
from src.slaves.bats2 import BATS2
from src.slaves.bats3_3b import BATS3_3B
from src.slaves.bats3_3a import BATS3_3A
from src.slaves.bats3_2b import BATS3_2B
from src.slaves.bats3_2a import BATS3_2A
from src.slaves.bats3_1b import BATS3_1B
from src.slaves.bats3_1a import BATS3_1A
from src.lin_peak import LinPeak

SOFTWARE = 0x01
CALIBRATION = 0x02


class VnvTool:
    """
    Main class which contain all abject of the tool such as
    Lin Driver, Power Supply driver, the client to communicate
    with the sensor and all configurations.
    """

    mQuit = False

    def __init__(self, swp_sx_file: str):
        self.conf_gen = None
        self.hw = False
        self.list_cal = None
        try:
            self.client = None
            self.conf_gen = VnvToolConf()
            self.list_cal = self.conf_gen.conf_get_battery_calib()
            self.param_setting(swp_sx_file)
            self.hw = self.client.bus_interface.hw

            if not self.hw:

                print("Please connect PeakLin Device ... \n")
                print("Quitting SWP \n")
            else:
                print('running')
                self.run(swp_sx_file)
        except Exception as e:
            print(f'Hardawre Connection Failed ==> {e}')
            # raise SwpError('Hardawre Connection Failed ..........')

    def param_setting(self, swp_sx_file):

        sw_cLient = ['BWC', 'BATS2', 'BATS3_1A', 'BATS3_2A', 'BATS3_3A', 'BATS3_1B', 'BATS3_2B', 'BATS3_3B']
        conf = self.conf_gen.vnv_conf
        retry = 1
        print("Software Package Parameter Settings ...\n")
        print("WARNINGS :  Battery Calibration ID have to be specified in INTEGER \n")
        client_set = str(input('Choose The Client eg ... BWC, BATS2, BATS3_XA, BATS3_XB : \n').upper())
        bat_id_set = int(input('Choose a calibration battery ID between the list above : \n'))
        while (bat_id_set not in self.list_cal):
            print(f"\n Wrong Calibration number chosen ... Retry Left {3 - retry}")
            bat_id_set = int(input('Choose a calibration battery ID in the list above : \n'))
            retry += 1
            if retry >=3 :
                print("1st Calibration is set as DEFAULT CALIBRATION")
                bat_id_set = int(self.list_cal[0])
                break

        if client_set not in sw_cLient or type(bat_id_set) != int:
            raise SwpError("Client not in The list \n")
        else:
            conf.client = client_set
            setattr(conf.swp_param, 'bat_id', bat_id_set)
            self.client = eval(f"{conf.client}()")  # create the client which communicate wit the sensor
            self.vnv_init(swp_sx_file)

    def vnv_init(self, swp_sx_file):

        # load the configuration file

        conf = self.conf_gen.vnv_conf
        bus = eval(f"{conf.bus_means}()")  # create concerning bus driver
        setattr(bus, "verbose", conf.verbose)  # set the verbose parameter
        setattr(self.client, "config", conf)  # load the configuration in the client
        try:
            setattr(bus, "bus_param", self.client.slave_conf.bus_param)  # load the parameters of the bus
            setattr(self.client, "bus_interface", bus)  # link the bus into the client
            self.client.clt_init(swp_sx_file)  # initialise the attributes of the client

            if self.client.bus_interface.li_initilize():
                self.hw = True  # initialise the attributes of the bus
            # del self.conf_gen, conf, bus, self.client.slave_conf  # delete all none used object for the RAM optimization
        except Exception as e:
            raise SwpError(str(e))


    def vnvt_dl_from_swp(self, dl_type):
        """ Download from SW Package

        Args:
            dl_type (int): type of DL, software or calibration

        Returns:
            bool: True if it is OK
        """
        if dl_type == SOFTWARE:
            return self.client.clt_sw_dl_from_swp()
        elif dl_type == CALIBRATION:
            if isinstance(self.client, BWC):
                return self.client.clt_calib_dl_from_swp(is_bwc=True)
            else:
                return self.client.clt_calib_dl_from_swp(is_bwc=False)

    def vnvt_dl_from_sw(self, dl_type):
        """ Download from SW 

        Args:
            dl_type (int): type of DL, software or calibration

        Returns:
            bool: True if it is OK
        """
        if dl_type == SOFTWARE:
            return self.client.clt_sw_dl_from_sw()
        elif dl_type == CALIBRATION:
            if isinstance(self.client, BWC):
                return self.client.clt_calib_dl_from_sw(is_bwc=True)
            else:
                return self.client.clt_calib_dl_from_sw(is_bwc=False)

    def vnvt_read_by_id(self, rid, param):
        """Send Frames to sensor for testing
        Args:

        Returns:
            bool :  True if it is OK
        """
        return self.client.clt_read_by_id(rid, param)

    def run(self, file: str):

        try:
            while not self.mQuit:
                time.sleep(0.5)
                print("############################################# \n")
                time.sleep(0.2)
                print("Software Package Main Menu \n")
                time.sleep(0.5)
                choice = input(
                    "Choose the action you want to perform : \n 1 - SW Download with Software Package \n 2 - Calibration "
                    "with SWP \n 3 - To Reset Parameter \n 4 - Read APP-CAL PN   \n ELSE - To Exit \n")
                time.sleep(0.5)
                if int(choice) == 1:
                    time.sleep(0.2)
                    print('Downloading the calibration with the specified parameter \n')
                    try:
                        assert self.vnvt_dl_from_swp(SOFTWARE) is True
                    except Exception as e:
                        raise SwpError(str(e))
                elif int(choice) == 2:
                    time.sleep(0.2)
                    print('Downloading the calibration with the specified parameter \n')
                    try:
                        assert self.vnvt_dl_from_swp(CALIBRATION) is True
                    except Exception as e:
                        raise SwpError(str(e))
                elif int(choice) == 3:
                    time.sleep(0.2)
                    print('Setting Tool Parameter')
                    try:
                        self.param_setting(file)
                    except Exception as e:
                        raise SwpError(str(e))
                elif int(choice) == 4:
                    time.sleep(0.2)
                    print('Setting Tool Parameter')
                    choice = input(
                        "----------------------------- \n 1 - Read Calibration Parameter : \n 2 - Read Software Parameter : \n")
                    if int(choice) == 1:
                        time.sleep(0.2)
                        print("Reading Calibration ID .... ")
                        try:
                            self.vnvt_read_by_id(0x24, [0xff, 0x7f, 0xff, 0xff])
                        except Exception as e:
                            raise SwpError(str(e))
                    elif int(choice) == 2:
                        time.sleep(0.2)
                        print("Reading Software App ID .... ")
                        try:
                            self.vnvt_read_by_id(0x22, [0xff, 0x7f, 0xff, 0xff])
                        except Exception as e:
                            raise SwpError(str(e))
                    else:
                        time.sleep(0.2)
                        print("Select a Valid Choice .... ")
                        # self.param_setting()
                else:
                    self.mQuit = True
                    time.sleep(0.2)
                    print("Quitting SWP \n")
                    break
        except Exception as e:
            raise SwpError(str(e))


def getOptions(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description="Parses command.")
    parser.add_argument("-i", "--input", help="Your input file.")
    option = parser.parse_args(args)
    return option


if __name__ == '__main__':
    options = getOptions(sys.argv[1:])
    # options.input = "data/sw_package.sx" # A enlever lors du buil de l'exe
    tool = VnvTool(options.input)
