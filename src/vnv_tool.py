#######################################################
#
# swp_tool.py
# Python implementation of the Class SwpTool
# Original author: ext_say
#
#######################################################

# Mandatory imported classes

from src.vnv_tool_config import VnvToolConf
from src.slaves.bwc import BWC
from src.slaves.bats2 import BATS2
from src.slaves.bats3_3b import BATS3_3B
from src.slaves.bats3_3a import BATS3_3A
from src.slaves.bats3_2b import BATS3_2B
from src.slaves.bats3_2a import BATS3_2A
from src.slaves.bats3_1b import BATS3_1B
from src.slaves.bats3_1a import BATS3_1A
from src.lin_peak import LinPeak

SOFTWARE = 0x01
CALIBRATION = 0x02


class VnvTool:
    """
    Main class which contain all abject of the tool such as
    Lin Driver, Power Supply driver, the client to communicate
    with the sensor and all configurations.
    """

    def __init__(self, swp_sx_file: str):
        conf_gen = VnvToolConf()  # load the configuration file
        conf = conf_gen.vnv_conf
        bus = eval(f"{conf.bus_means}()")  # create concerning bus driver
        setattr(bus, "verbose", conf.verbose)  # set the verbose parameter
        self.client = eval(f"{conf.client}()")  # create the client which communicate wit the sensor
        setattr(self.client, "config", conf)  # load the configuration in the client
        setattr(bus, "bus_param", self.client.slave_conf.bus_param)  # load the parameters of the bus
        setattr(self.client, "bus_interface", bus)  # link the bus into the client
        self.client.clt_init(swp_sx_file)  # initialise the attributes of the client
        self.client.bus_interface.li_initilize()  # initialise the attributes of the bus
        del conf_gen, conf, bus, self.client.slave_conf  # delete all none used object for the RAM optimization

    def vnvt_dl_from_swp(self, dl_type):
        """ Download from SW Package

        Args:
            dl_type (int): type of DL, software or calibration

        Returns:
            bool: True if it is OK
        """
        if dl_type == SOFTWARE:
            return self.client.clt_sw_dl_from_swp()
        elif dl_type == CALIBRATION:
            if isinstance(self.client, BWC):
                return self.client.clt_calib_dl_from_swp(is_bwc=True)
            else:
                return self.client.clt_calib_dl_from_swp(is_bwc=False)

    def vnvt_dl_from_sw(self, dl_type):
        """ Download from SW 

        Args:
            dl_type (int): type of DL, software or calibration

        Returns:
            bool: True if it is OK
        """
        if dl_type == SOFTWARE:
            return self.client.clt_sw_dl_from_sw()
        elif dl_type == CALIBRATION:
            if isinstance(self.client, BWC):
                return self.client.clt_calib_dl_from_sw(is_bwc=True)
            else:
                return self.client.clt_calib_dl_from_sw(is_bwc=False)

    def vnvt_read_by_id(self, rid, param):
        """ Read By ID

        Args:
            rid (int): read ID
            param (list): parameters

        Returns:
            bool: True if it is OK
        """
        return self.client.clt_read_by_id(rid, param)

    def vnvt_ecureset(self):
        """Ecu Reset
        Args:

        Returns:
            bool: True if it is OK
        """
        return self.client.clt_ecu_reset()

    def vnvt_rdba(self, addr, nb_bytes):
        """Read data by address
        Args:
        Returns:
            bool: True if it is OK
        """
        return self.client.clt_read_memory_by_addr(addr, nb_bytes)

    def vnvt_wdba(self, data, addr, size):
        """Write data by address
        Args:
        Returns:
            bool: True if it is OK
        """
        return self.client.clt_write_memory_by_addr(data, addr, size)

    def vnvt_diagSess(self, session):
        """Diagnostic Session Control
        Args:

        Returns:
            bool: True if it is OK
        """
        return self.client.clt_diagnostic_session_ctl(session)

    def vnvt_security_access(self, rid, data):
        """ Security access
        Args:

        Returns:
            bool: True if it is OK
        """

        return self.client.clt_security_access(rid, data)

    def vnvt_erase_cal_memory(self, bwc):
        """Routine Control Erase Cal
        Args:
             bwc: bool
        Returns:
            bool : True if it is OK
            """

        return self.client.clt_erase_cal_memory(bwc)

    def vnvt_erase_app_memory(self):
        """Routine Control Erase App
        Args:
             bwc: bool
        Returns:
            bool : True if it is OK
            """

        return self.client.clt_erase_app_memory()

    def vnvt_rtcl_request_value(self, param):
        """Routine Control Request Result
                Args:
                     param:
                Returns:
                    bool : True if it is OK
                    """

        return self.client.clt_rtcl_request_result(param)

    def vnvt_send_frames(self, sid, subfonction, size, request):
        """Send Frames to sensor for testing
        Args:

        Returns:
            bool :  True if it is OK
        """
        return self.client.clt_send_frames(sid, subfonction, size, request)

    def vnvt_request_download(self, addr, size):
        """Send Frames to sensor for testing
        Args:

        Returns:
            bool :  True if it is OK
        """
        return self.client.clt_request_download(addr, size)
