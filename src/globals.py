import time
import json
from contextlib import contextmanager
import enum

# from lin_dcm import *
from ctypes import *

class SESSION(enum.Enum):
    DCM_SS_DEFAULT = 0x01  # Defaultsession
    DCM_SS_PROGRAMMING = 0x02  # LEM Boot session(programming session)
    DCM_SS_EXTENDED = 0x03  # Extended session
    DCM_SS_LEM = 0x60  # LEM Calibration session
    DCM_SS_TEST = 0x61  # LEM test session
    DCM_SS_CUSTOMER = 0x63  # Customer session
    DCM_SS_SUPPLIER = 0x64  # / * Supplier session


class Singleton(object):
    """ Class to create a single object

    Args:
        object (class): empety object
    Returns:
        object: single object
    """
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = object.__new__(cls)
        return cls._instance


class obj:
    """ Class to create an object from a dictionnary
    """

    def __init__(self, dico):
        self.__dict__.update(dico)


def dict2obj(dico: dict):
    """
    this function translate a dict to an object and
    all its children tree
    dico : dictionary
    :object tree
    """
    # using json.loads method and passing json.dumps
    # method and custom object hook as arguments
    return json.loads(json.dumps(dico), object_hook=obj)


def step_wait(period):
    """ Decorator to send a wakeup frame if the bus is in sleep mode
        before send the frame, and wait a period of time

    Args:
        period (float): time to the wait
    """

    def send_lin(fonction):
        def ret_function(self, *param, **param2):
            self.bus_interface.li_wakeup_slave()
            ret_code = fonction(self, *param, **param2)
            time.sleep(period)
            return ret_code

        return ret_function

    return send_lin


def send_wait(period):
    """ Decorator to wait a period of time after each sending frame

    Args:
        period (float): time to the wait (20 ms between two frame)
    """

    def send_func(fonction):
        def ret_function(self, *param, **param2):
            ret_code = fonction(self, *param, **param2)
            time.sleep(period)
            return ret_code

        return ret_function

    return send_func


@contextmanager
def execution_time(execution_msg):
    """ Context manager to use  with statment to calculate and 
        print the time execution of a function

    Args:
        execution_msg (str): string to print specific message or function name
    """
    start_t = time.time()
    yield
    end_t = time.time()
    print(f"Execution time of {execution_msg} : {end_t - start_t}")


######################################################################################
ls_crc16tab = [
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
    0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
    0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
    0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
    0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
    0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
    0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
    0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
    0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
    0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
    0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
    0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
    0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
    0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
    0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
    0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
    0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
    0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
    0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
    0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
    0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
    0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
    0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
    0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
    0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
    0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
    0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
    0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
    0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
]

CRC_MAX_DATASIZE = 131070


######################################################################################
# SECURITY COMPUTING
def applyxor(data, phrase):
    phrase = int.from_bytes(phrase, byteorder='big', signed=False)
    for i in range(0, len(data)):
        j = i % 4
        data[i] = int(data[i]) ^ (phrase >> (8 * (3 - j)))


def _crc16(data, crc, table):
    for byte in data:
        # crc = ((crc << 8) & 0xff00) ^ table[((crc >> 8) & 0xff) ^ byte]
        index = crc >> 8
        index = index ^ byte
        crc = ((crc & 0x00ff) << 8) ^ table[index]
    return crc


def crc16xmodem(data, crc=0):
    a = []
    key = [0, 0, 0, 0]
    crcc = _crc16(data, crc, ls_crc16tab)
    for i in range(len(data) // 2):
        key[2 * i] = (crcc >> 8)
        key[2 * i + 1] = (crcc & 0x00FF)

    for item in key:
        a.append(int(item))
    return a


def computekey(data, session):
    a = []
    code = ''
    dc = []
    if session == SESSION.DCM_SS_LEM.value:
        xorphrase = bytes([0x5A, 0x5A, 0xC3, 0xC3])
    elif session == SESSION.DCM_SS_PROGRAMMING.value:
        xorphrase = bytes([0xCA, 0xFE, 0xF0, 0x0D])
    elif session == SESSION.DCM_SS_CUSTOMER.value:
        xorphrase = bytes([0xC3, 0xA5, 0x5A, 0x3C])
    elif session == SESSION.DCM_SS_TEST.value:
        xorphrase = bytes([0xB1, 0x6A, 0x55, 0xE5])
    elif session == SESSION.DCM_SS_EXTENDED.value:
        xorphrase = bytes([0xF0, 0x07, 0xFA, 0xCE])
    elif session == SESSION.DCM_SS_SUPPLIER.value:
        xorphrase = bytes([0xFC, 0xBA, 0xFA, 0xBE])

    key = crc16xmodem(data, crc=0xffff)
    applyxor(key, xorphrase)
    for item in key:
        a.append(hex(int(item)))
    for x in a:
        if 'x' not in x[-2]:
            code = f'{code}' + x[-2:]
            b = '0x' + x[-2:]
        else:
            code = f'{code}' + x[-2:]
            b = '0x0' + x[-1:]
        dc.append(int(b, 16))
    return dc


######################################################
# TEST SECURITY

def dvtest_Crc_CalculateCRC16(self, Crc_DataPtr, Crc_Length, Crc_StartValue16, Crc_IsFirstCall):
    # Initialise the Crc calculation
    if Crc_IsFirstCall == True:
        Crc = 0xFFFF
    else:
        Crc = Crc_StartValue16

    if None != Crc_DataPtr:
        # check CRC length, to avoid polyspace defect (loop bounded with tainted value)
        if ((Crc_Length > 0) and (Crc_Length < CRC_MAX_DATASIZE)):
            # Calculate CRC for the complete length
            for i in range(Crc_Length):
                index = Crc >> 8
                # polyspace <DEFECT:TAINTED_PTR:Low:Justified> The pointer is not null, and its size is passed in the parameters. It is up to the caller to provide a pointer with that size allocated */
                # polyspace <MISRA-C3:D4.14:Low:Justified> The pointer is not null, and its size is passed in the parameters. It is up to the caller to provide a pointer with that size allocated */
                index = index ^ Crc_DataPtr[i]
                Crc = ((Crc & 0x00FF) << 8) ^ ls_crc16tab[index]
    return Crc


def dvtest_Crc_computeKey(self, p_seed, p_key, keySize):
    keyCrc = 0xFFFF
    idxData = 0
    keyCrc = self.dvtest_Crc_CalculateCRC16(p_seed, keySize, keyCrc, False)
    if ((None != p_key) and (keySize > 0) and (keySize < 128)):

        for idxData in range(int(keySize / 2)):
            # polyspace +2 DEFECT:TAINTED_PTR [Justified:Low] "The pointer is not null, and its size is passed in the parameters. It is up to the caller to provide a pointer with that size allocated" */
            # polyspace +2 MISRA-C3:D4.14 [Justified:Low] "The pointer is not null, and its size is passed in the parameters. It is up to the caller to provide a pointer with that size allocated" */
            p_key[0][2 * idxData] = keyCrc >> 8
            p_key[0][2 * idxData + 1] = keyCrc & 0x00FF


def dvtest_computeSAKey(self, session, seed):
    res = (c_ubyte * 4)(0, 0, 0, 0)
    tmpSeed = list()
    ret = 0
    tmpSeed.append((seed & 0xFF000000) >> 24)
    tmpSeed.append((seed & 0x00FF0000) >> 16)
    tmpSeed.append((seed & 0x0000FF00) >> 8)
    tmpSeed.append(seed & 0x000000FF)

    self.dvtest_Crc_computeKey(tmpSeed, pointer(res), 4)

    for c, i in zip(res, range(4)):
        ret |= c << 8 * (3 - i)

    if session == SESSION.DCM_SS_LEM.value:
        ret = ret ^ 0x5A5AC3C3
    elif SESSION.DCM_SS_PROGRAMMING.value:
        ret = ret ^ 0xCAFEF00D
    elif session == SESSION.DCM_SS_CUSTOMER.value:
        ret = ret ^ 0xC3A55A3C
    elif session == SESSION.DCM_SS_TEST.value:
        ret = ret ^ 0xB16A55E5
    elif session == SESSION.DCM_SS_EXTENDED.value:
        ret = ret ^ 0xF007FACE
    elif session == SESSION.DCM_SS_SUPPLIER.value:
        ret = ret ^ 0xFCBAFABE
    else:
        ret = seed
    return ret


if __name__ == '__main__':
    a = [0xF3, 0xD3, 0x5E, 0xFD]
    b = computekey(a, SESSION.DCM_SS_LEM.value)
    for u in b:
        print(hex(u))
