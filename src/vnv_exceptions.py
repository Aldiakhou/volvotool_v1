#######################################################
#
# lin_interface.py
# Python implementation of the Class Exceptions
# Original author: ani
#
#######################################################
class SwpError(Exception):

    def __init__(self, msg):
        self.msg = f'SWP EROOR : {msg}'


class LinError(Exception):

    def __init__(self, msg):
        self.msg = f'LIN ERROR : {msg}'


class LinTpError(Exception):

    def __init__(self, msg):
        self.msg = f'LinTp ERROR : {msg}'


class DcmError(Exception):

    def __init__(self, msg):
        self.msg = f'DCM ERROR : {msg}'


class TestExecError(Exception):

    def __init__(self, msg):
        self.msg = f'Test ERROR : {msg}'
