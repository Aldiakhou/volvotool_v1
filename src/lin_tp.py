#######################################################
#
# lin_interface.py
# Python implementation of the Class LinInterface
# Original author: ani
#
#######################################################
import logging

from src.globals import send_wait, Singleton
from src.lin_dcm import LinDcm, LINTP_BUFFERSIZE
from src.vnv_exceptions import LinTpError

LINTP_SF_MAXSIZE = 6
LINTP_FF_MAXSIZE = 5
LINTP_CF_MAXSIZE = 6
LINTP_SF = 0
LINTP_FF = 1
LINTP_CF = 2


class LinTp(Singleton):
    """ LIN transport layer 

    Args:
        Singleton (class): to create single object

    Returns:
        object: Lin Tp layer
    """
    lin = None  # reference of Lin driver

    def __init__(self, lin):
        LinTp.lin = lin  # LIN driver
        self.nad = lin.bus_param.nad  # NAD of the target sensor
        self.dcm = LinDcm(self)  # create the DCM object layer
        self.lintp_pdu = [0] * 8  # PDU farme buffer 
        self.cntCF = 0x21  # concecutive frame counter
        self.cntRemainingBytes = 0  # remaining bytes counter
        self.ret_resp = False

    def ltp_flush(self):
        """ to empty the buffer of the low layer bus
        """
        self.lin.li_flush()

    @send_wait(0.5)  # wait 500 ms
    def ltp_trasmit(self, buffer, size):
        """ Function to transmit the data from 'buffer'

        Args:
            buffer (list): list of data bytes
            size (int): size of the data

        Returns:
            bool: return True if the transmission is OK else False
        """
        ret_code = False
        if buffer is not None:
            if size <= LINTP_BUFFERSIZE + 8:
                if size <= LINTP_SF_MAXSIZE:
                    ret_code = self.ltp_sendSF(buffer, size)  # single frame
                else:
                    ret_code = self.ltp_sendMF(buffer, size)  # Multiple frames
        return ret_code

    def ltp_receive(self, sid_resp):
        """ Send a slaveResp to receive the response from sensor

        Args:
            sid_resp (int): sid + 0x40

        Returns:
            bool: True if the response is OK
        """
        rx_buffer = []
        try:
            self.lin.li_flush()  # clear the low layer buffer
            ret_code = self.lin.li_write(self.lin.slaveResp, self.lin.EMPTY_FRAME)  # send a slaveResp
            if ret_code is not True:
                raise LinTpError("SlaveResp Lin Transmission KO")
            rx_buffer, self.ret_resp = self.lin.li_read(self.lin.slaveResp, sid_resp)  # read the corresponding response
            if rx_buffer:  # check if there is a response
                if rx_buffer[0] != self.nad:  # check the NAD
                    raise LinTpError(f"NAD: {hex(self.nad)} is not good")
                elif (rx_buffer[1] & 0xF0) == 0x10:  # if there is consecutive frames
                    length = (rx_buffer[1] & 0x0F << 8) + rx_buffer[2]  # calculate the length of data to read
                    read = 4  # 4 bytes for already read in the first frame
                    while read < length - 1:
                        self.lin.li_flush()
                        ret_code = self.lin.li_write(self.lin.slaveResp, self.lin.EMPTY_FRAME)  # send a slaveResp
                        if ret_code is not True:
                            raise LinTpError("SlaveResp Lin Transmission KO")
                        buffer, self.ret_resp = self.lin.li_read(self.lin.slaveResp,
                                                                 sid_resp)  # read the corresponding response
                        if self.ret_resp:  # check if there is a response
                            size = min(length - read, 6)
                            read += size
                            rx_buffer += buffer[2:size + 2]
                            return rx_buffer[3:]
                else:
                    self.ret_resp = False
            else:
                self.ret_resp = False

            return rx_buffer[2:]

        except Exception as ex:
            logging.exception(str(ex))

    def ltp_sendMF(self, buffer, size):
        """ send several frames 

        Args:
            buffer (list): data to send from buffer
            size (int): size of data
        Returns:
            bool: True if the transmission is OK
        """
        ret_code = self.ltp_sendFF(buffer, size)  # send First Frame
        if ret_code:
            while self.cntRemainingBytes > 0:
                ret_code |= self.ltp_sendCF(buffer[size - self.cntRemainingBytes:])  # send concecutive frame
        else:
            raise LinTpError("ltp_sendFF")
        return ret_code

    def ltp_sendSF(self, buffer, size):
        """ Send a single frame

        Args:
            buffer (list): data to send from buffer
            size (int): size of data

        Returns:
            bool: True if transmission is OK
        """
        self.lintp_pdu[0] = self.nad
        self.lintp_pdu[1] = size
        self.__copy_from_list(buffer, 8 - LINTP_SF_MAXSIZE, size)  # copy data from buffer in the pdu
        self.__padding_frame(LINTP_SF_MAXSIZE, size)  # padding the reste with 0
        ret_code = False
        try:
            ret_code = self.lin.li_write(self.lin.masterReq, self.lintp_pdu)  # write in the LIN
            if ret_code is not True:
                raise LinTpError("MasterReq Lin Transmission KO")
        except Exception as ex:
            logging.exception(str(ex))
        return ret_code

    def ltp_sendFF(self, buffer, size):
        """ Send First Frame

        Args:
            buffer (list): data to send from buffer
            size (int): size of data

        Returns:
            bool: True if transmission is OK
        """
        self.lintp_pdu[0] = self.nad
        # second byte, upper 4 bits is the frame type (FF), lower 4 bits are the upper 4 bits of the total size
        self.lintp_pdu[1] = (LINTP_FF << 4) | ((size & 0x0F00) >> 8)  # FF=0x1 | size
        # second byte is the lower 8 bits of the total size
        self.lintp_pdu[2] = size & 0x00FF
        self.__copy_from_list(buffer, 8 - LINTP_FF_MAXSIZE, LINTP_FF_MAXSIZE)  # copy data from buffer in the pdu
        self.__padding_frame(LINTP_SF_MAXSIZE, size)  # padding the reste with 0
        ret_code = False
        try:
            ret_code = self.lin.li_write(self.lin.masterReq, self.lintp_pdu)  # write in the LIN
            if ret_code is not True:
                raise LinTpError("MasterReq Lin Transmission KO")
            self.cntRemainingBytes = size - LINTP_FF_MAXSIZE  # calculate the remaining data to send
            self.cntCF = 0x21
        except Exception as ex:
            logging.exception(str(ex))
        return ret_code

    def ltp_sendCF(self, buffer):
        """ Send the concecutive frames

        Args:
            buffer (list): data to send from buffer

        Returns:
            bool: True if transmission is OK
        """
        # increment the CF frame counter (modulo 16)
        self.lintp_pdu[0] = self.nad
        # frame type 2 is on the 4 upper bits of first byte, the CF counter (starting at 1) is on the lower 4 bits
        self.lintp_pdu[1] = self.cntCF  # CF | SN
        ret_code = False
        if self.cntRemainingBytes > LINTP_CF_MAXSIZE:
            szMsg = LINTP_CF_MAXSIZE
        else:
            szMsg = self.cntRemainingBytes
        self.__copy_from_list(buffer, 8 - LINTP_CF_MAXSIZE, szMsg)  # copy data from buffer in the pdu
        self.__padding_frame(LINTP_SF_MAXSIZE, szMsg)  # padding the reste with 0
        try:
            ret_code = self.lin.li_write(self.lin.masterReq, self.lintp_pdu)  # write in the LIN
            if ret_code is not True:
                raise LinTpError("MasterReq Lin Transmission KO")
            self.cntRemainingBytes -= szMsg
            self.cntCF += 1  # increase the CF counter
        except Exception as ex:
            logging.exception(str(ex))
        # Modification for  the new software download procedure
        if self.cntCF > 0x2F:  # This block is to be removed for the old software package
            self.cntCF = 0x20
        return ret_code

    def __copy_from_list(self, list_src, offset, size):
        """ Copy 'size' bytes from 'offset' of 'list_src' in 'self.lintp_pdu'

        Args:
            list_src (list): buffer source
            offset (int): offset from where it copy bytes
            size (int): size of data to copy
        """
        for i in range(size):
            self.lintp_pdu[offset + i] = list_src[i]

    def __padding_frame(self, max_data, size: int):
        """ Padding ith 0 the reste of data in 'self.lintp_pdu'

        Args:
            max_data (int): max data in the buffer
            size (int): size of the good data
        """
        if size < max_data:
            lenf = max_data - size
            for i in range(lenf):
                self.lintp_pdu[size + (8 - max_data) + i] = 0x00
