#######################################################
#
# lin_interface.py
# Python implementation of the Class LinInterface
# Original author: ani
#
#######################################################
import json
import logging
import enum
import yaml
import binascii as ba
import time
from src.globals import Singleton, obj
from src.vnv_exceptions import DcmError

ECU_HARDRESET = 0x01  # ResetType : always hardReset as this is the only resetType supported
MAX_NB_RETRY_TRANSFERDATA = 4
ROUTINECTRL_TYPESTART = 0x01
LINTP_BUFFERSIZE = 512
LINTP_RETRY = 10


class SESSION(enum.Enum):
    DCM_SS_DEFAULT = 0x01  # Defaultsession
    DCM_SS_PROGRAMMING = 0x02  # LEM Boot session(programming session)
    DCM_SS_EXTENDED = 0x03  # Extended session
    DCM_SS_LEM = 0x60  # LEM Calibration session
    DCM_SS_TEST = 0x61  # LEM test session
    DCM_SS_CUSTOMER = 0x63  # Customer session
    DCM_SS_SUPPLIER = 0x64  # / * Supplier session


class LinDcm(Singleton):
    """ DCM Layer

    Args:
        Singleton (class): to create single object
    """

    def __init__(self, lintp):
        self.lin_tp = lintp  # to link with LinTp layer
        self.rx_buffer = None  # used as RX Buffer
        self.tx_buffer = None  # used as TX buffer
        self.pn_data = None
        with open("conf/dcm_params.yml", "r") as fd:  # load the DCM parameters
            data = fd.read()
            param = yaml.load(data, Loader=yaml.FullLoader)
        self.dcm_param = json.loads(json.dumps(param), object_hook=obj)  # translate to an object tree

    def dcm_ecu_reset(self):
        """ Build an ECU reset frame and send it

        Returns:
            bool: True if it is OK
        """
        func_info = "ECU Reset"
        self.tx_buffer = [self.dcm_param.service.DCM_SID_ECURESET, ECU_HARDRESET]
        return self.dcm_send_message(self.dcm_param.service.DCM_SID_ECURESET, size=2,
                                     func_info=func_info, p_retry=3)

    def dcm_diagnostic_session_ctl(self, session):
        """ Build a diagnostic session control frame and send it

        Args:
            session (int): session type to open
        Returns:
            bool: True if it is OK
        """
        func_info = "Diagnostic Session Control"
        self.tx_buffer = [self.dcm_param.service.DCM_SID_DIAGSSCONTROL, session]
        return self.dcm_send_message(self.dcm_param.service.DCM_SID_DIAGSSCONTROL,
                                     2, func_info)

    def dcm_erase_app_memory(self):
        """  Send a Routine Control frame to erase Application memory

        Returns:
            bool: True if it is OK
        """
        ret_code = False
        try:
            ret_code = self.dcm_routine_ctl(self.dcm_param.sub_function.start_routine,
                                            self.dcm_param.routine_id.erase_memory,
                                            self.dcm_param.routine_ctl_option_record.erase_app_mem, 5)
            if ret_code is not True:
                raise DcmError("Routine Control : Erase App memory")
        except Exception as ex:
            logging.exception(str(ex))
        return ret_code

    def dcm_erase_cal_memory(self, bwc):
        """  Send a Routine Control frame to erase Calibration memory

        Args:
            bwc (bool): if it is bwc
        Returns:
            bool: True if it is OK
        """
        ret_code = False
        if bwc:
            self.lin_tp.lin.li_wakeup_slave()
            self.lin_tp.ltp_flush()
            calib_mode = self.dcm_param.routine_id.erase_bwc_memory
        else:
            calib_mode = self.dcm_param.routine_id.erase_memory
        try:
            ret_code = self.dcm_routine_ctl(self.dcm_param.sub_function.start_routine,
                                            calib_mode,
                                            self.dcm_param.routine_ctl_option_record.erase_cal_mem, 5)
            if ret_code is not True:
                raise DcmError("Routine Control : Erase Calibration memory")
        except Exception as ex:
            logging.exception(str(ex))
        return ret_code

    def dcm_routine_ctl_service(self, param):
        """Send Routine Control to read value in the Sensor
        Args:

        Returns:
            bool: True if it is OK"""
        ret_code = False
        try:
            ret_code = self.dcm_routine_ctl(self.dcm_param.sub_function.start_routine,
                                            self.dcm_param.routine_id.request_value,
                                            param, 6)
            if ret_code is not True:
                raise DcmError("Routine Control : Request Result")
        except Exception as ex:
            logging.exception(str(ex))
        try:
            ret_code = self.dcm_routine_ctl(self.dcm_param.sub_function.request_Routine_Results,
                                            self.dcm_param.routine_id.request_value,
                                            param, 6)
            if ret_code is not True:
                raise DcmError("Routine Control : Request Result")
        except Exception as ex:
            logging.exception(str(ex))
        return ret_code

    def dcm_request_download(self, addr, size):
        """  Build a Request Download frame and send it

        Args:
            addr (int): address where the data will be send
            size (int): size of data
        Returns:
            bool: True if it is OK
        """
        func_info = 'Request Download'
        self.tx_buffer = [self.dcm_param.service.DCM_SID_REQUESTDWNLD, 0x00, 0x45, 0x01]
        self.tx_buffer += addr.to_bytes(4, byteorder='big')
        self.tx_buffer += size.to_bytes(4, byteorder='big')
        return self.dcm_send_message(self.dcm_param.service.DCM_SID_REQUESTDWNLD,
                                     12, func_info)

    def dcm_transfer_data(self, data, size):
        """  Build a Transfer Download frames and send them

        Args:
            data (list): data which is send
            size (int): size of data
        Returns:
            bool: True if it is OK for all
        """
        func_info = 'Transmission Data'
        self.lin_tp.lin.li_print(f'IN {func_info} .... wait for transfer finish')
        remain_data_size = size
        bloc_size = min(remain_data_size, LINTP_BUFFERSIZE)
        offset_start = 0
        offset_end = bloc_size
        bloc_count = 1
        ret_code = True
        try:
            while remain_data_size > 0:
                self.tx_buffer = [self.dcm_param.service.DCM_SID_TRANSFERDATA, bloc_count]
                self.tx_buffer += data[offset_start:offset_end]
                ret_code = self.dcm_send_message(self.dcm_param.service.DCM_SID_TRANSFERDATA,
                                                 bloc_size + 2, func_info, bloc_count)
                bloc_count += 1
                remain_data_size -= bloc_size
                bloc_size = min(remain_data_size, LINTP_BUFFERSIZE)
                offset_start = offset_end
                offset_end += bloc_size
                if not ret_code:
                    raise DcmError('Transfer Data suspended due to error in Download Process')
        except Exception as ex:
            logging.exception(str(ex))
        return ret_code

    def dcm_transfer_exit(self):
        """  Build a Transfer Exit frame and send it

        Returns:
            bool: True if it is OK for all
        """
        func_info = 'Transfer Exit'
        self.tx_buffer = [self.dcm_param.service.DCM_SID_REQUESTTRSFREXIT]
        return self.dcm_send_message(self.dcm_param.service.DCM_SID_REQUESTTRSFREXIT,
                                     1, func_info)

    def dcm_read_by_id(self, rid, param):
        """  Build a Read By ID frame and send it

        Args:
            rid (int): Read  ID
            param (list): parameters
        Returns:
            bool: True if it is OK for all
        """
        func_info = 'Read By ID'
        self.tx_buffer = [self.dcm_param.service.DCM_SID_READBYID, rid]
        self.tx_buffer += param
        return self.dcm_send_message(self.dcm_param.service.DCM_SID_READBYID,
                                     6, func_info)

    def dcm_routine_ctl(self, ctl_type, routine_id, param, size):
        """ Build a Routine Control frame and send it

        Args:
            ctl_type (int): control type
            routine_id (int): routine ID
            param (int): parameters
            size (int): size of data

        Returns:
            bool: True if it is OK for all
        """
        func_info = 'Routine Control'
        self.tx_buffer = [self.dcm_param.service.DCM_SID_ROUTINECTRL, ctl_type]
        self.tx_buffer += routine_id.to_bytes(2, byteorder='big')
        self.tx_buffer += param.to_bytes(size - 4, byteorder='big')
        sid_resp = self.dcm_param.service.DCM_SID_ROUTINECTRL + 0x40
        nb_retry = 0
        if ROUTINECTRL_TYPESTART == ctl_type:
            length = size
        else:
            length = 6

        ret_code = self.dcm_send_message(self.dcm_param.service.DCM_SID_ROUTINECTRL,
                                         length, func_info, p_retry=10)
        try:
            if ret_code:
                if sid_resp != self.rx_buffer[0]:  # if the sid response is not ok
                    while nb_retry < LINTP_RETRY:  # retry
                        self.rx_buffer = self.lin_tp.ltp_receive(sid_resp)
                        if self.rx_buffer and \
                                sid_resp != self.rx_buffer[1]:
                            ret_code = True
                            break
                        else:
                            ret_code = False
                            nb_retry += 1
                    if nb_retry == LINTP_RETRY and not ret_code and self.rx_buffer:
                        self.lin_tp.lin.li_print(
                            "DCM Routine Control negative response received. NRC = {0:#x}.".format(self.rx_buffer[2]))
            else:
                self.lin_tp.lin.li_print("DCM Routine Control: Request message not sent!")
        except Exception as ex:
            logging.exception(str(ex))
        return ret_code

    def dcm_write_memory_by_addr(self, data, addr, size):
        """ Build a Write Memory By Address frame and send it

        Args:
            data (list): data to send 
            addr (int): address where the data will be write
            size (int): size of data

        Returns:
            bool: True if it is OK for all
        """
        func_info = "Write Memory By Address"
        self.tx_buffer = [self.dcm_param.service.DCM_SID_WRMEMBYADDR, 0x24]
        self.tx_buffer += addr.to_bytes(4, byteorder='big')
        self.tx_buffer += size.to_bytes(2, byteorder='big')
        self.tx_buffer += data
        return self.dcm_send_message(self.dcm_param.service.DCM_SID_WRMEMBYADDR,
                                     size + 8, func_info, addr)

    def dcm_read_memory_by_addr(self, addr, nb_bytes):
        """ Build a Read Memory By Address frame and send it

        Args:
            addr (int): address where the data will be read
            nb_bytes (int): Number of byte to read

        Returns:
            bool: True if it is OK for all
        """
        func_info = "Read Memory By Address"
        self.tx_buffer = [self.dcm_param.service.DCM_SID_RDMEMBYADDR, 0x24]
        self.tx_buffer += addr.to_bytes(4, byteorder='big')
        self.tx_buffer += nb_bytes.to_bytes(2, byteorder='big')
        ret_code = self.dcm_send_message(self.dcm_param.service.DCM_SID_RDMEMBYADDR,
                                         8, func_info)
        if ret_code:
            self.lin_tp.lin.li_print(
                f"DCM {func_info}, Value read = {0}".format(
                    ba.hexlify(bytearray(self.rx_buffer[1:nb_bytes + 1]))))
        return ret_code

    def dcm_check_transmit(self, nrc, sid_resp, str_func, p_retry):
        """ Function that check the validity of the response 

        Args:
            nrc (int): Negative Response Code
            sid_resp (int): serviceID + 0x40
            str_func (str): function description string to print
            p_retry (int): number max of retry

        Returns:
             bool: True if it is OK for all
        """
        ret_code = False
        nb_retry = 0
        self.rx_buffer = None
        try:
            while (nrc == self.dcm_param.nrc.BRR or nrc == self.dcm_param.nrc.RCRRP) and \
                    nb_retry < p_retry:  # while nrc == 0x21 or 0x78 and number of retry is not max
                self.rx_buffer = self.lin_tp.ltp_receive(sid_resp)
                while not self.rx_buffer and nb_retry < p_retry:  # rerty until there is response and retry max
                    self.rx_buffer = self.lin_tp.ltp_receive(sid_resp)
                    nb_retry += 1
                if len(self.rx_buffer) > 0:
                    if self.rx_buffer[0] == self.dcm_param.nrc.SNSIAS and \
                            self.rx_buffer[1] == sid_resp - 0x40:  # if there is a negative response
                        nrc = self.rx_buffer[2]
                        if nrc == self.dcm_param.nrc.BRR:  # if nrc 0x21 then retry
                            nb_retry += 1
                        elif nrc == self.dcm_param.nrc.RCRRP:  # if nrc == 0x78 then retry for Pending action
                            nb_retry = 0
                            ret_code = False
                        else:
                            ret_code = False
                            self.lin_tp.lin.li_print(
                                f'DCM {str_func} negative response received with NRC : 0x%02X' % self.rx_buffer[2])
                            break

                    elif self.rx_buffer[0] == sid_resp:  # if there is positive response
                        ret_code = True
                        if sid_resp in [0x63, 0xF2]:
                            self.pn_data = self.rx_buffer
                        break
                    else:
                        ret_code = False
                        nb_retry += 1
                else:
                    self.lin_tp.lin.li_print(
                        f'DCM {str_func} No response received from DCM')
        except Exception as ex:
            logging.exception(str(ex))
        return ret_code

    def dcm_send_message(self, sid, size, func_info, other=None, p_retry=LINTP_RETRY):
        """[summary]

        Args:
            sid (int): Service ID
            size (int): size of data 
            func_info (str): function information
            other (T, optional): list or number to print. Defaults to None.
            p_retry (int, optional): number max of retry. Defaults to LINTP_RETRY.

        Returns:
            bool: True if it is OK for all
        """
        ret_code = False
        sid_resp = sid + 0x40
        nrc = self.dcm_param.nrc.BRR
        self.lin_tp.lin.li_print(f'DCM {func_info} Message ...')
        while p_retry > 0 and ret_code == False:
            p_retry -= 1
            try:
                self.lin_tp.ltp_flush()
                ret_code = self.lin_tp.ltp_trasmit(self.tx_buffer, size)
                if sid == 0x11:
                    time.sleep(0.02)
                if ret_code:
                    ret_code = self.dcm_check_transmit(nrc,
                                                       sid_resp, func_info,
                                                       5)
                    if ret_code:
                        comp_msg = ''
                        if other is not None:
                            comp_msg = f'for : 0x%06X' % other
                        self.lin_tp.lin.li_print(
                            f'DCM {func_info} positive response received {comp_msg}')
                    else:
                        self.lin_tp.lin.li_print(
                            f'DCM {func_info} check transmission KO')
                else:
                    ret_code = False
                    raise DcmError(f'{func_info} Transmit message')

            except Exception as ex:
                logging.exception(str(ex))
        return ret_code

    def dcm_security_access(self, Srequest, key):
        """ Security Access
        Args:
            Srequest (int): Request type SendKey or RequestSeed
            key  (list): data


        Returns:
            bool: True if it is OK for all
        """
        nb_retry = 10
        func_info = "Security Access"
        ret_code = False
        nrc = self.dcm_param.nrc.CNC

        sid_resp = self.dcm_param.service.DCM_SID_SECURITYACCESS + 0x40
        if Srequest == self.dcm_param.sub_function.sa_reqseed:
            self.tx_buffer = [self.dcm_param.service.DCM_SID_SECURITYACCESS, Srequest]
            size = 2

        elif Srequest == self.dcm_param.sub_function.sa_sendkey:
            self.tx_buffer = [self.dcm_param.service.DCM_SID_SECURITYACCESS, Srequest]
            self.tx_buffer += key
            size = 6

        else:
            DcmError("Requested Service Not Supported ")

        ret_code = self.dcm_send_message(self.dcm_param.service.DCM_SID_SECURITYACCESS,
                                         size, func_info)

        try:
            if ret_code:
                if sid_resp != self.rx_buffer[0]:  # if the sid response is not ok
                    while nb_retry < LINTP_RETRY:  # retry
                        self.rx_buffer = self.lin_tp.ltp_receive(sid_resp)
                        if self.rx_buffer and sid_resp != self.rx_buffer[1]:
                            ret_code = True
                            seed = self.rx_buffer[2:]
                            for i in range(len(seed) - 1):
                                key[i] = seed[i]
                            break
                        else:
                            ret_code = False
                            nb_retry += 1
                    if nb_retry == LINTP_RETRY and not ret_code and self.rx_buffer:
                        self.lin_tp.lin.li_print(
                            "DCM Security Access negative response received. NRC = {0:#x}.".format(
                                self.rx_buffer[2]))
                else:
                    if self.rx_buffer and sid_resp != self.rx_buffer[1]:
                        ret_code = True
                        seed = self.rx_buffer[2:]
                        print('seed', seed)
                        for i in range(len(seed)):
                            key[i] = seed[i]
                        nb_retry = 10
            else:
                self.lin_tp.lin.li_print("DCM Security Access: Request message not sent!")
        except Exception as ex:
            logging.exception(str(ex))
        if Srequest == self.dcm_param.sub_function.sa_reqseed:
            self.lin_tp.lin.li_print(f"DCM {func_info}, Value read = {0:#x}".format(
                ba.hexlify(bytearray(self.rx_buffer[1:]))))
        return ret_code

    def dcm_send_frames(self, sid, subfonction, size, request):
        self.tx_buffer = [sid, subfonction]
        if size <= 2:
            self.tx_buffer += request.to_bytes(size, byteorder='big')
        else:
            self.tx_buffer += request.to_bytes(size - 2, byteorder='big')
        func_info = 'DCM Send Frames'
        if sid == 0x36:
            return self.dcm_send_message(sid, size, func_info, 1)
        else:
            return self.dcm_send_message(sid, size, func_info, p_retry=3)
