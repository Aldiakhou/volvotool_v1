#######################################################
#
# bats3_1a.py
# Python implementation of the Class BATS3_1A
# Original author: ext_say
#
#######################################################
import yaml

from src.globals import dict2obj
from src.lin_client import LinClient


class BATS3_1A(LinClient):
    """This class reprensents Bats3_1A communication properties

    Args:
        LinClient: Class to define a Lin client
    """

    def __init__(self):
        super()
        super().__init__()
        with open("conf/bats3_1A_conf.yml", "r") as fd:
            data = fd.read()
            conf = yaml.load(data, Loader=yaml.FullLoader)
        self.slave_conf = dict2obj(conf)
