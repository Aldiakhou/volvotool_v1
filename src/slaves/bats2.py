#######################################################
#
# bats2.py
# Python implementation of the Class BATS2
# Original author: ext_say
#
#######################################################
import yaml

from src.globals import dict2obj
from src.lin_client import LinClient


class BATS2(LinClient):
    """This class reprensents Bats2 communication properties

    Args:
        LinClient: Class to define a Lin client 
    """

    def __init__(self):
        super().__init__()
        with open("conf/bats2_conf.yml", "r") as fd:
            data = fd.read()
            conf = yaml.load(data, Loader=yaml.FullLoader)

        self.slave_conf = dict2obj(conf)
