#######################################################
#
# bats3_3a.py
# Python implementation of the Class BATS3_3A
# Original author: ext_say
#
#######################################################
import yaml

from src.globals import dict2obj
from src.lin_client import LinClient


class BATS3_3A(LinClient):
    """This class reprensents Bats3_3A communication properties

    Args:
        LinClient: Class to define a Lin client
    """

    def __init__(self):
        super()
        super().__init__()
        with open("conf/bats3_3A_conf.yml", "r") as fd:
            data = fd.read()
            conf = yaml.load(data, Loader=yaml.FullLoader)
        self.slave_conf = dict2obj(conf)
