#######################################################
#
# bwc.py
# Python implementation of the Class BWC
# Original author: ext_say
#
#######################################################
import yaml

from src.globals import dict2obj
from src.lin_client import LinClient


class BWC(LinClient):
    """This class reprensents Bwc communication properties

    Args:
        LinClient: Class to define a Lin client
    """

    def __init__(self):
        super()
        super().__init__()
        with open("conf/bwc_conf.yml", "r") as fd:
            data = fd.read()
            conf = yaml.load(data, Loader=yaml.FullLoader)
        self.slave_conf = dict2obj(conf)
