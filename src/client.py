#######################################################
#
# lin_interface.py
# Python implementation of the Class Client
# Original author: ani
#
#######################################################
from abc import ABC, abstractmethod
import bincopy as bc
import binascii as ba
from src.vnv_exceptions import SwpError
import logging




class Client(ABC):
    """ This class contains all common attributs and functions
        and the abstrat functions to define a client

    Args:
        ABC: To create an Abstract class
    """

    def __init__(self):
        self.bat_cal = None
        self.config = None  # contains all configuration from tool_conf.yml file
        self.swp_head = None  # Header Package properties
        self.swp_start_addr = None  # SW Package start address (starting with application)
        self.swp_sr_bin_file = None  # S-record binary file of SW Package
        self.swp_head_size = None  # Header package size
        self.swp_header = None  # Header package data
        self.swp_app_size = None  # Application size in SW Package
        self.sw_app_size = None  # Application size in SW "TEST"
        self.swp_cal_size = None  # Calibration size in SW Package
        self.swp_app_addr = None  # Application start address in embedded memory
        self.swp_cal_addr = None  # Calibration start address in embedded memory
        self.swp_tn_cal = None  # Total Number of Calibrations
        self.sw_start_addr = None  # Application start address in sx file
        self.sw_sr_bin_file = None  # S-record binary file of SW
        self.cal_sr_bin_file = None  # S-record binary file of calibration
        self.it_vector_size = None  # Interrupt vector segment size
        self.calib_tab = None

    def clt_print(self, val):
        """ This function print the traces if the verbose
            mode is activated

        Args:
            val (bool): to activate the verbose mode (True)
        """
        if self.config.verbose:
            print(val)

    def clt_init(self, swp_sx_file: str):
        """ This function initialize all attributs
        """
        self.swp_head = self.config.swp_param.package_header
        self.swp_start_addr = self.config.swp_param.swp_start_addr
        # self.swp_sr_bin_file = bc.BinFile(self.config.swp_param.swp_file_name)

        self.swp_sr_bin_file = bc.BinFile(swp_sx_file)
        self.sw_start_addr = self.config.sw_param.app_start_addr
        # self.sw_sr_bin_file = bc.BinFile(self.config.sw_param.sw_file_name)
        # self.cal_sr_bin_file = bc.BinFile(self.config.sw_param.calib_file_name)
        self.swp_head_size = sum([s.size for s in self.swp_head.__dict__.values()])
        self.swp_header = self.swp_sr_bin_file.as_binary(minimum_address=self.swp_start_addr,
                                                         maximum_address=self.swp_start_addr + self.swp_head_size)
        self.swp_app_size = self.clt_binlist_to_int(self.swp_header, self.swp_head.SZ_SW.offset,
                                                    self.swp_head.SZ_SW.size)
        self.swp_cal_size = self.clt_binlist_to_int(self.swp_header, self.swp_head.SZ_1_CAL.offset,
                                                    self.swp_head.SZ_1_CAL.size)
        self.swp_app_addr = self.config.app_start_addr
        self.swp_cal_addr = self.config.calib_start_addr
        self.swp_tn_cal = self.swp_header[self.swp_head.TN_CAL.offset]
        self.it_vector_size = self.config.swp_param.app_it_vector_size
        self.sw_app_size = self.config.sw_param.app_size
        self.bat_cal = self.config.bat_cal.xls_file_name


    @staticmethod
    def clt_binlist_to_int(lst, offset, size):
        """ Translate a list of bytes (4 bytes for 32 bits system) to an integer 

        Args:
            lst (list): list of bytes
            offset (int): offset in the list
            size (int): number of bytes (max 4 for 32 bits system)

        Returns:
            int: the traslated number
        """
        return int.from_bytes(lst[offset:offset + size], "big")

    def clt_get_swp_app(self):
        """ Exctract the Application from the SW Package to download

        Returns:
            list: list of bytes represents the binary data of the application
        """
        return self.swp_sr_bin_file.as_binary(minimum_address=self.swp_start_addr + self.swp_head_size,
                                              maximum_address=self.swp_start_addr +
                                                              self.swp_head_size +
                                                              self.swp_app_size)

    def clt_get_swp_calib(self):
        """ Exctract the Calibration from the SW Package to download

        Returns:
            list: list of bytes represents the binary data of the calibration
        """

        bat_id = self.config.swp_param.bat_id  # get the battery type id
        min_addr = self.swp_start_addr + self.swp_head_size + self.swp_app_size
        max_addr = min_addr + self.swp_cal_size
        for c in range(self.swp_tn_cal):  # find in the SWP, the corresponding calibration
            self.calib_tab = self.swp_sr_bin_file.as_binary(minimum_address=min_addr,
                                                       maximum_address=max_addr)
            if self.calib_tab[self.config.swp_param.bat_id_offset] == bat_id:
                break
            else:
                min_addr += self.swp_cal_size
                max_addr += self.swp_cal_size
                #del self.calib_tab
        return self.calib_tab


    # def clt_get_sw_app(self):
    #     """ Exctract the Application from the SW S-record file to download
    #
    #     Returns:
    #         list: list of bytes represents the binary data of the application
    #     """
    #     return self.sw_sr_bin_file.as_binary(minimum_address=self.sw_start_addr,
    #                                          maximum_address=self.sw_start_addr +
    #                                                          self.swp_app_size)

    def clt_get_swp_pn(self):
        """ Exctract the PN from the SWP S-record file to download

        Returns:
            list: list of bytes represents the binary data of the application
        """
        return self.clt_binlist_to_int(self.swp_header, self.swp_head.P_PN.offset,
                                       self.swp_head.P_PN.size -1)

    def clt_get_swp_calbwc_pn(self):
        """ Exctract the PN from the SWP S-record file to download

        Returns:
            list: list of bytes represents the binary data of the application
        """
        calib_tab = self.clt_get_swp_calib()
        return int.from_bytes(calib_tab[4:4 + 4] + calib_tab[0:1 + 1] + calib_tab[6:6 + 6], "big")

    def clt_get_swp_cal_pn(self):
        """ Exctract the PN from the SWP S-record file to download

        Returns:
            list: list of bytes represents the binary data of the application
        """
        calib_tab = self.clt_get_swp_calib()
        return str(hex(int.from_bytes(calib_tab[0:0 + 1], "big"))) + '00' +str(int.from_bytes(calib_tab[3:3 + 1], 'big'))

    # def clt_get_sw_calib(self):
    #     """ Exctract the Calibration from the SW S-record file to download
    #
    #     Returns:
    #         list: list of bytes represents the binary data of the calibration
    #     """
    #     return self.cal_sr_bin_file.as_binary(minimum_address=self.config.sw_param.cal_start_addr,
    #                                           maximum_address=self.config.sw_param.cal_start_addr +
    #                                                           self.config.sw_param.cal_size)

    def clt_check_crc16(self, data):
        """ Calculate the CRC16 of data -2 bytes and compare
            that with the CRC16 stored in the end of data if it
            exist, otherwise it stored the new CRC in the end.

        Args:
            data (list): list of binary bytes

        Returns:
            list: data with good CRC16
        """
        crc_new = ba.crc_hqx(data[:-2], 0xFFFF)
        crc_old = self.clt_binlist_to_int(data[-2:], 0, 2)
        if crc_old != crc_new:
            # data[-2] = crc_new.to_bytes(2, byteorder='big')[0]
            # data[-1] = crc_new.to_bytes(2, byteorder='big')[1]
            raise SwpError("Error at CRC Calculation, Please Check the SW Package")
        return data

    def clt_check_swp_crc(self):
        """Check the CRC32 of SWP

        Returns:
            bool: True if CRC32 is good else False
        """
        data = self.swp_sr_bin_file.as_binary(minimum_address=self.swp_start_addr + self.swp_head_size,
                                              maximum_address=self.swp_start_addr +
                                                              self.swp_app_size * self.swp_tn_cal +
                                                              self.swp_cal_size)
        crc_new = ba.crc32(data)
        crc_old = self.clt_binlist_to_int(self.swp_header,
                                          self.swp_head.CRC.offset,
                                          self.swp_head.CRC.size)
        del data
        return crc_old == crc_new

    def clt_sw_dl_from_swp(self):
        """ SW download from SW Package

        Raises:
            SwpError: SW Package exception

        Returns:
            bool: return True if the SW download has gone well
        """
        try:
            sw_dl = True
            check_swp = self.clt_check_swp_crc()  # check crc32 of SWP
            if check_swp:
                app_data = self.clt_get_swp_app()  # exctract Application from SWP
                app_data = self.clt_check_crc16(app_data[:-self.it_vector_size]) + \
                           app_data[-self.it_vector_size:]  # check the crc16 of Appliction and add the interrupt vector
                calib_data = self.clt_get_swp_calib()  # exctract Calibration from SWP
                calib_data = self.clt_check_crc16(calib_data)  # check the crc16
                sw_dl = self.clt_sw_dl(calib_data + app_data)
                # New Software Download Procedure Integration SWDL with APP ONLY
                # sw_dl = self.clt_sw_dl(app_data)  # do a SW download
            else:
                raise SwpError("SWP CRC32 KO")
        except Exception as ex:
            logging.exception(str(ex))
        return sw_dl

    def clt_calib_dl_from_swp(self, is_bwc: bool):
        try:
            cal_dl = True
            check_swp = self.clt_check_swp_crc()  # check crc32 of SWP
            if check_swp:
                calib_data = self.clt_get_swp_calib()  # extract Calibration from SWP
                calib_data = self.clt_check_crc16(calib_data)  # check the crc16
                if is_bwc:
                    calib_data = self.clt_get_swp_calib()  # extract Calibration from SWP
                    calib_data = self.clt_check_crc16(calib_data)  # check the crc16
                    cal_dl = self.clt_bwc_calib_dl(calib_data, is_bwc)  # do a calibration download for BWC
                else:
                    cal_dl = self.clt_calib_dl(calib_data, is_bwc)  # do a calibration download for other
            else:
                raise SwpError("SWP CRC32 KO")
        except Exception as ex:
            logging.exception(str(ex))
        return cal_dl

    # def clt_sw_dl_from_sw(self):
    #     try:
    #         sw_dl = True
    #         app_data = self.clt_get_sw_app()  # extract Application from SW
    #         app_data = self.clt_check_crc16(app_data[:-self.it_vector_size]) + \
    #                    app_data[-self.it_vector_size:]  # check the crc16 of Application and add the interrupt vector
    #         # calib_data = self.clt_get_sw_calib()  # extract Calibration from SW
    #         # calib_data = self.clt_check_crc16(calib_data)  # check the crc16
    #         # sw_dl = self.clt_sw_dl(calib_data + app_data)
    #         # New Software Download Procedure Integration SWDL with APP ONLY
    #         sw_dl = self.clt_sw_dl(app_data)  # do a SW download
    #     except Exception as ex:
    #         logging.exception(str(ex))
    #     return sw_dl
    #
    # def clt_calib_dl_from_sw(self, is_bwc: bool):
    #     try:
    #         cal_dl = True
    #         calib_data = self.clt_get_sw_calib()  # extract Calibration from SW
    #         check_calib = self.clt_check_crc16(calib_data)  # check the crc16
    #         if check_calib:
    #             if is_bwc:
    #                 cal_dl = self.clt_bwc_calib_dl(calib_data, is_bwc)  # do a calibration download for BWC
    #             else:
    #                 cal_dl = self.clt_calib_dl(calib_data, is_bwc)  # do a calibration download for other
    #         else:
    #             raise SwpError("CALIB CRC16 KO")
    #     except Exception as ex:
    #         logging.exception(str(ex))
    #     return cal_dl

    @abstractmethod
    def clt_calib_dl(self, data: list, is_bwc: bool):
        """ Calibration Download

        Args:
            data (list): list of binary bytes to download
            is_bwc (bool): True if BWC
        """

    @abstractmethod
    def clt_bwc_calib_dl(self, data: list, is_bwc: bool):
        """ Calibration download for BWC

        Args:
            data (list): list of binary bytes to download
            is_bwc (bool): True if BWC
        """

    @abstractmethod
    def clt_sw_dl(self, data: list):
        """ Software (APP + CALIB) Download

        Args:
            data (list): list of binary bytes to download
        """

    @abstractmethod
    def clt_ecu_reset(self):
        """ Send an ECU Reset and check the response
        """

    @abstractmethod
    def clt_diagnostic_session_ctl(self):
        """ Send a diagnostic session control and check the response
        """

    @abstractmethod
    def clt_erase_app_memory(self):
        """ Send an erase application memory and check the response
        """

    @abstractmethod
    def clt_erase_cal_memory(self):
        """ Send an erase calibration memory and check the response
        """

    @abstractmethod
    def clt_request_download(self, addr: list, size: int):
        """[summary]

        Args:
            addr (list): address where the data will transfer
            size (int): size of the data to transfer
        """

    @abstractmethod
    def clt_transfer_data(self, data: list, addr: int, size: int):
        """ Send a transfer data and check the response

        Args:
            data (list): list of binary bytes to transfer
            addr (int): address where the data will transfer
            size (int): size of the data
        """

    @abstractmethod
    def clt_transfer_exit(self):
        """ Send a transfer exit and check the response
        """

    @abstractmethod
    def clt_read_by_id(self, pid: int):
        """ Send a read by ID and check the response

        Args:
            pid (int): data ID to read
        """
