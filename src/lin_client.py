#######################################################
#
# lin_client.py
# Python implementation of the Class LinClient
# Original author: ani
#
#######################################################
import json
import logging
import time
from abc import ABC
from src.globals import step_wait, send_wait
from src.lin_dcm import SESSION
from src.client import Client
from src.vnv_exceptions import LinError
from src.lin_tp import LINTP_BUFFERSIZE


class LinClient(Client, ABC):
    """ Class to create a Lin client object

    Args:
        Client (class): to define a client with its functions
        ABC (class): to create an abstract class

    """
    bus_interface = None  # bus interface object (CAN or LIN)
    slave_conf = None  # contain all properties of one target sensor
    sensor_data = None

    def clt_bwc_calib_dl(self, data, is_bwc: bool):
        print("BWC Download calibration")
        print("######################################")
        ret_code = False
        try:

            ret_code = self.clt_diagnostic_session_ctl(SESSION.DCM_SS_PROGRAMMING.value)
            if ret_code:
                ret_code = self.clt_ecu_reset()
            if ret_code:
                ret_code = self.clt_erase_cal_memory(is_bwc)
            else:
                raise LinError("ECUReset start KO")
            if ret_code:
                ret_code = self.clt_write_memory_by_addr_loop(data)
            else:
                raise LinError("EraseCalMemory KO")
            if ret_code:
                address = (self.config.bwc_calib_pn_addr & 0x00FFFF) | 0x090000  # send to address begin with 0x09
                # Correction Download for BATS2 BWC
                num_bytes = 0x10
                ret_code = self.clt_read_memory_by_addr(address, num_bytes)
            else:
                raise LinError("WriteMemoryByAddress KO")
            if ret_code:
                self.clt_check_swp_caldlbwc_ok()
            else:
                raise LinError("ReadMemoryByAddress KO")
            if ret_code is not True:
                LinError("Calibration Info Verification Failed")
        except Exception as ex:
            logging.exception(str(ex))
        print("######################################")
        return ret_code

    def clt_calib_dl(self, data, is_bwc: bool):
        print("Download calibration")
        print("######################################")
        ret_code = False
        try:
            ret_code = self.clt_diagnostic_session_ctl(SESSION.DCM_SS_PROGRAMMING.value)
            if ret_code :
                ret_code = self.clt_ecu_reset()
            if ret_code:
                ret_code = self.clt_diagnostic_session_ctl(SESSION.DCM_SS_PROGRAMMING.value)
            else:
                raise LinError("ECUReset start KO")
            if ret_code:
                ret_code = self.clt_erase_cal_memory(is_bwc)
            else:
                raise LinError("DiagnosticSession KO")
            if ret_code:
                ret_code = self.clt_request_download(self.swp_cal_addr,
                                                     self.swp_cal_size)
            else:
                raise LinError("EraseCalMemory KO")
            if ret_code:
                ret_code = self.clt_transfer_data(data,
                                                  self.swp_cal_size)
            else:
                raise LinError("RequestDownload KO")
            if ret_code:
                ret_code = self.clt_transfer_exit()
            else:
                raise LinError("TransfertData KO")
            if ret_code:
                ret_code = self.clt_ecu_reset()
            else:
                raise LinError("TransfertExit KO")
            if ret_code:
                rid = 0x24
                param = [0xFF, 0x7F, 0xFF, 0xFF]
                size = 4
                ret_code = self.clt_read_by_id(rid, param)
            else:
                raise LinError("ECUReset end KO")
            if ret_code is not True:
                raise LinError('Error in SW Download Procedure')
        except Exception as ex:
            logging.exception(str(ex))
        print("######################################")
        return ret_code

    def clt_sw_dl(self, data):
        print("Download application")
        print("######################################")
        ret_code = False
        rid = 0x22
        param = [0xFF, 0x7F, 0xFF, 0xFF]
        bparam = [0xF1, 0x94, 0xFF, 0xFF]
        pn = ''
        size = 5
        try:
            ret_code = self.clt_diagnostic_session_ctl(SESSION.DCM_SS_PROGRAMMING.value)
            if ret_code:
                ret_code = self.clt_ecu_reset()
            else:
                raise LinError("Download Initialisation KO")
            if ret_code:
                ret_code = self.clt_diagnostic_session_ctl(SESSION.DCM_SS_PROGRAMMING.value)
            else:
                raise LinError("ECUReset start KO")
            if ret_code:
                ret_code = self.clt_erase_app_memory()
            else:
                raise LinError("DiagnosticSession KO")
            if ret_code:
                ret_code = self.clt_erase_cal_memory(False)
            else:
                raise LinError("EraseCalMemory KO")
            if ret_code:
                # Request Download for New Procedure APP ONLY
                ret_code = self.clt_request_download(self.swp_cal_addr, self.swp_cal_size +
                                                     self.swp_app_size)
                # ret_code = self.clt_request_download(self.swp_app_addr, self.swp_app_size)
            else:
                raise LinError("EraseAppMemory KO")
            if ret_code:
                ret_code = self.clt_transfer_data(data,
                                                  self.swp_cal_size +
                                                  self.swp_app_size)
                # ret_code = self.clt_transfer_data(data, self.swp_app_size)
                if not ret_code:
                    raise LinError("TransfertData of Software KO")
            else:
                raise LinError("RequestDownload KO")
            if ret_code:
                ret_code = self.clt_transfer_exit()
            else:
                raise LinError("TransfertData KO")
            if ret_code:
                ret_code = self.clt_ecu_reset()
            else:
                raise LinError("TransfertExit KO")
            if ret_code:
                time.sleep(0.5)
                ret_code = self.clt_read_by_id(rid, param)
            else:
                raise LinError("ECUReset end KO")

            if ret_code is not True:
                raise LinError("ReadByID KO")
        except Exception as ex:
            logging.exception(str(ex))
        print("######################################")
        return ret_code

    @step_wait(1)
    def clt_ecu_reset(self):
        # wakeup slave
        ret_code = self.bus_interface.lin_dcm.dcm_ecu_reset()
        if ret_code is not True:
            ret_code = False
        return ret_code

    @step_wait(1)
    def clt_diagnostic_session_ctl(self, session):
        ret_code = self.bus_interface.lin_dcm.dcm_diagnostic_session_ctl(session)
        if ret_code is not True:
            ret_code = False
        return ret_code

    @step_wait(1)
    def clt_erase_app_memory(self):
        ret_code = self.bus_interface.lin_dcm.dcm_erase_app_memory()
        if ret_code is not True:
            ret_code = False
        return ret_code

    @step_wait(0.5)
    def clt_erase_cal_memory(self, bwc: bool):
        ret_code = self.bus_interface.lin_dcm.dcm_erase_cal_memory(bwc)
        if ret_code is not True:
            ret_code = False
        return ret_code

    @step_wait(0.02)
    def clt_request_download(self, addr, size):
        ret_code = self.bus_interface.lin_dcm.dcm_request_download(addr, size)
        if ret_code is not True:
            ret_code = False
        return ret_code

    @step_wait(0.05)
    def clt_transfer_data(self, data, size):
        ret_code = self.bus_interface.lin_dcm.dcm_transfer_data(data, size)
        if ret_code is not True:
            ret_code = False
        return ret_code

    @step_wait(0.02)
    def clt_transfer_exit(self):
        ret_code = self.bus_interface.lin_dcm.dcm_transfer_exit()
        if ret_code is not True:
            ret_code = False
        return ret_code

    @step_wait(0.05)
    def clt_read_by_id(self, rid, param):
        ret_code = self.bus_interface.lin_dcm.dcm_read_by_id(rid, param)
        self.sensor_data = self.bus_interface.lin_dcm.pn_data
        if self.sensor_data :
            print("\n SENSOR DATA :", self.sensor_data)
        if ret_code is not True:
            ret_code = False
        return ret_code

    @step_wait(0.05)
    def clt_write_memory_by_addr(self, data, addr, size):
        ret_code = self.bus_interface.lin_dcm.dcm_write_memory_by_addr(data, addr, size)
        if ret_code is not True:
            ret_code = False
        return ret_code

    @step_wait(0.05)
    def clt_read_memory_by_addr(self, addr, nb_bytes):
        ret_code = self.bus_interface.lin_dcm.dcm_read_memory_by_addr(addr, nb_bytes)
        self.sensor_data = self.bus_interface.lin_dcm.pn_data
        if ret_code is not True:
            ret_code = False
        return ret_code

    @step_wait(0.05)
    def clt_write_memory_by_addr_loop(self, data):
        ret_code = True
        remain_data_size = self.swp_cal_size
        bloc_size = min(remain_data_size, LINTP_BUFFERSIZE)
        offset_start = 0
        offset_end = bloc_size
        addr = (self.swp_cal_addr & 0x00FFFF) | 0x090000
        try:
            while remain_data_size > 0:
                ret_code = self.clt_write_memory_by_addr(data[offset_start:offset_end],
                                                         addr,
                                                         bloc_size)
                if ret_code:
                    addr += bloc_size
                    remain_data_size -= bloc_size
                    bloc_size = min(remain_data_size, LINTP_BUFFERSIZE)
                    offset_start = offset_end
                    offset_end += bloc_size
                else:
                    ret_code = False
                    raise LinError("Write Memory By Address")
        except Exception as ex:
            logging.exception(str(ex))
        return ret_code

    @step_wait(0.05)
    def clt_security_access(self, rid, data):
        ret_code = self.bus_interface.lin_dcm.dcm_security_access(rid, data)
        if ret_code is not True:
            ret_code = False
        return ret_code

    def clt_rtcl_request_result(self, param):
        ret_code = self.bus_interface.lin_dcm.dcm_routine_ctl_service(param)
        if ret_code is not True:
            ret_code = False
        return ret_code

    def clt_send_frames(self, sid, subfonction, size, request):
        ret_code = self.bus_interface.lin_dcm.dcm_send_frames(sid, subfonction, size, request)
        if ret_code is not True:
            ret_code = False
        return ret_code

    def clt_check_swp_swdl_ok(self):
        pn = ''
        ret_code = False
        swp_pn = hex(self.clt_get_swp_pn())

        if self.sensor_data is not None:
            for i in self.sensor_data:
                i = hex(i)
                pn += i[2:]
            pn = f'0x{pn[2:]}'
            if swp_pn in pn:
                ret_code = True
        else:
            print("Couldn't Retrieve the value of Sensor PN")
            ret_code = False
        print('SWP PN: ', swp_pn, 'SENSOR PN', pn)
        return ret_code

    def clt_check_swp_caldlbwc_ok(self):
        pn = ''
        ret_code = False
        swp_pn = hex(self.clt_get_swp_calbwc_pn())
        if self.sensor_data is not None:
            for i in self.sensor_data:
                i = hex(i)
                pn += i[2:]
            pn = f'0x{pn[2:]}'
            if swp_pn in pn:
                ret_code = True
        else:
            print("Couldn't Retrieve the value of Sensor PN")
            ret_code = False
        print('SWP PN: ', swp_pn, '\nSENSOR PN', pn)
        return ret_code

    def clt_check_swp_caldl_ok(self):
        pn = ''
        ret_code = False
        swp_pn = self.clt_get_swp_cal_pn()
        print("SWP", swp_pn)
        if self.sensor_data is not None:
            for i in self.sensor_data:
                i = hex(i)
                pn += i[2:]
            pn = f'0x{pn[2:]}'
            if str(swp_pn) in pn:
                ret_code = True
        else:
            print("Couldn't Retrieve the value of Sensor PN")
            ret_code = False
        print('CAL PN: ', swp_pn, 'SENSOR PN', pn, ret_code)
        return ret_code