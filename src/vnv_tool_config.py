#######################################################
#
# vnv_tool_config.py
# Python implementation of the Class VnvToolConf
# Original author: ext_say
#
#######################################################
import yaml
import openpyxl
from src.globals import dict2obj


class VnvToolConf:

    def __init__(self):
        print("\n***************************************************** \n")
        print("\n************ LEM-VOLVO SW PACKAGE TOOL V1 *********** \n")
        print("\n***************************************************** \n")
        with open("conf/tool_conf.yml", "r") as fd:

            data = fd.read()
            conf = yaml.load(data, Loader=yaml.FullLoader)
        self.vnv_conf = dict2obj(conf)

    def conf_get_battery_calib(self):
        try:
            print("############################################# \n")
            wb_obj = openpyxl.load_workbook("conf/BATT_ID_TYPE.xlsx")
            act_sheet = wb_obj.active
            # print("Row",act_sheet.max_row,"col", act_sheet.max_column)
            print("\nAvailable Battery Calibration")
            liste = list()
            for i in range(4, act_sheet.max_row + 1):
                cell_obj = act_sheet.cell(row=i, column=1)
                cell_obj1 = act_sheet.cell(row=i, column=3)
                if cell_obj1.value :
                    print("ID",cell_obj.value,"Calib name",cell_obj1.value,"\n")
                    liste.append(eval(cell_obj.value))

        except Exception as e:
            print("Provide BATT_ID_TYPE.xlsx to the data Folder")
            print(e)
        else :
            return liste
