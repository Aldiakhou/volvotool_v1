#######################################################
#
# lin_nixnet.py
# Python implementation of the Class LinNiXnet
# Original author: ani
#
#######################################################

# import logging
# import copy
# from globals import *
# import six
# import nixnet
# from nixnet import constants
# from dataclasses import dataclass, field
# from nixnet import types

from globals import send_wait
from lin_interface import LinInterface
import enum
from vnv_exceptions import LinError


class LinNixnet(LinInterface):
    """ Class to define all low layer function of LIN bus driver

        Args:
            LinInterface (class): Class which define the LIN bus object
            with all functions to define in the low layer, and contains the LIN TP and DCM layers
        """

    def __init__(self):
        super()
        self.rx_buffer = []
        self.lp_initialize()
        pass

    def lp_initialize(self):
        pass

    def lp_uninitialize(self):
        pass

    def li_read(self, **kwargs):
        pass

    def li_wakeup_slave(self):
        pass

    def li_write(self, fid: int, frame: list):
        pass

    def li_print(self, val: str):
        pass
