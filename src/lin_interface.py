#######################################################
#
# lin_interface.py
# Python implementation of the Class LinInterface
# Original author: ani
#
#######################################################
import logging
from abc import ABC, abstractmethod
from src.lin_tp import LinTp
from src.lin_dcm import LinDcm
from src.globals import Singleton




class LinInterface(ABC, Singleton):
    """ Class to define a specific bus interface object (CAN or LIN)

    Args:
        ABC (class): to create an abstract class
        Singleton (class): to create a single object
    """
    bus_param = None  # contains the bus properties and parameters
    verbose = None  # to print the traces  (True)
    lin_tp = None  # Object which contains the Lin Transport layer and DCM object layer
    masterReq = None  # Frame ID of masterReq (0x3C)
    slaveResp = None  # # Frame ID of staveResp (0x3D)
    EMPTY_FRAME = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]  # empty frame to wakeup the sensor

    def li_print(self, val: str):
        """ This function print the traces if the verbose
            mode is activated

        Args:
            val (bool): to activate the verbose mode (True)
        """
        if self.verbose:
            print(val)


    def li_initilize(self):
        """ To initialise the attributs
        """
        LinInterface.lin_tp = LinTp(self)
        LinInterface.lin_dcm = LinDcm(LinInterface.lin_tp)
        self.masterReq = self.bus_param.frame_ids.masterReq
        self.slaveResp = self.bus_param.frame_ids.slaveResp

    @abstractmethod
    def li_wakeup_slave(self):
        """ To send a wakeup to the sensor
        """

    @abstractmethod
    def li_read(self, fram_id: int, sid_resp: int):
        """ Send a slaveResp or unconditional frame to read a specific message and check
            it with sid_resp (sid + 0x40)

        Args:
            fram_id (int): frame ID
            sid_resp (int): sid + 0x40
        """

    @abstractmethod
    def li_write(self, fid: int, frame: list):
        """ Send a frame to a sensor defined with the frame id 'fid'

        Args:
            fid (int): the frame ID
            frame (list): frame to send
        """
